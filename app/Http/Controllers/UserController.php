<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => ['required','email','unique:users'],
            'confirm_email' => ['required','same:email'],
            'password' => ['required'],
            'confirm_password' => ['required','same:password']
        ]);
        $hash = Hash::make($request->password);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $hash
        ]);
        return response()->json($user, 201);
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'email' => ['required','email'],
            'password' => ['required']
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user){
            return response()->json(['message' => 'Invalid credentials'], 401);
        }
        if (!Hash::check($request->password, $user->password)) {
            //seperated so softlocks can be implemented at a later date
            return response()->json(['message' => 'Invalid credentials'], 401);
        }
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json(['token' => $token], 200);
    }
}
