<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ToDo;
use Illuminate\Http\Request;

class ToDoController extends Controller
{
    /**
     * Display a listing of the resource related to a user and a project (optional)
     */
    public function index(Request $request)
    {
        $toDos = ToDo::where('user_id', $request->user()->id);
        if($request->project){
            $toDos = $toDos->where('project_id', $request->project);
        }
        return response()->json($toDos->get(), 200);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Project $project)
    {
        $projectController = new ProjectController();
        $projectController->isAuthorized($request, $project);
        $request->validate([
            'description' => ['required', 'string', 'max:255']
        ]);
        $toDo = $project->toDos()->create([
            'description' => $request->description,
            'user_id' => $request->user()->id
        ]);
        return response()->json($toDo, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, ToDo $toDo)
    {
        $this->isAuthorized($request, $toDo);
        $toDo->increment('views');
        return response()->json($toDo, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ToDo $toDo)
    {
        $this->isAuthorized($request, $toDo);
        $request->validate([
            'description' => ['string', 'max:255'],
            'status' => ['in:Todo,Done']
        ]);
        if(isset($request->description)){
            $toDo->description = $request->description;
        }
        if(isset($request->status)){
            $toDo->status = $request->status;
        }
        $toDo->save();
        return response()->json($toDo, 200);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, ToDo $toDo)
    {
        $this->isAuthorized($request, $toDo);
        $toDo->delete();
    }

    /**
     * Check if the user is authorized to perform the action.
     */
    private function isAuthorized(Request $request, ToDo $toDo)
    {
        if($toDo->user_id !== $request->user()->id){
            abort(403);
        }
        return;
    }
}
