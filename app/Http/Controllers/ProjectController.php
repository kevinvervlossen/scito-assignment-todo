<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return Project::where('user_id', $request->user()->id)->orWhereNull('user_id')->get();
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'private' => ['required', 'boolean']
        ]);
        $userId = $request?->private ? $request->user()->id : null;
        $project = Project::create([
            'name' => $request->name,
            'user_id' => $userId
        ]);
        return response()->json($project, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, Project $project)
    {
        $this->isAuthorized($request, $project);
        $project->load('toDos');
        return response()->json($project, 200);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Project $project)
    {
        $this->isAuthorized($request, $project);
        $request->validate([
            'name' => ['string', 'max:255'],
            'private' => ['boolean']
        ]);
        if(isset($request->private)){
            if($request->private){
                $project->user_id = $request->user()->id;
            } else {
                $project->user_id = null;
            }
        }
        if(isset($request->name)){
            $project->name = $request->name;
        }
        $project->save();
        return response()->json($project, 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, Project $project)
    {
        $this->isAuthorized($request, $project);
        $project->delete();
        return response()->json(null, 204);
    }

    public function isAuthorized(Request $request, Project $project): void
    {
        //normally this would be a policy
        if($project->user_id === null){
            return;
        }
        if($project->user_id === $request->user()->id){
            return;
        }
        return abort(403, 'Unauthorized');
    }
}
