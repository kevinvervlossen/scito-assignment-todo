<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ToDoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/register', [UserController::class, 'store']);
// The throttle middleware is used to limit the number of requests to the login endpoint.
Route::middleware('throttle:5,1')->post('/login', [UserController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::prefix('projects')->group(function () {
        Route::get('/', [ProjectController::class, 'index']);
        Route::post('/', [ProjectController::class, 'store']);
        Route::get('/{project}', [ProjectController::class, 'show']);
        Route::put('/{project}', [ProjectController::class, 'update']);
        Route::delete('/{project}', [ProjectController::class, 'destroy']);
        Route::post('/{project}/to-dos', [ToDoController::class, 'store']);
    });

    Route::prefix('to-dos')->middleware('auth:sanctum')->group(function () {
        Route::get('/', [ToDoController::class, 'index']);
        Route::get('/{toDo}', [ToDoController::class, 'show']);
        Route::put('/{toDo}', [ToDoController::class, 'update']);
        Route::delete('/{toDo}', [ToDoController::class, 'destroy']);
    });

});
