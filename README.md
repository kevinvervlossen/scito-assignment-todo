## Assignment

The purpose of this assignment is to have a working API in Laravel. The request is to make a simple ToDo manager. (Only the API, no frontend). The basic requirements are that a user can make projects and create todos in the project. A project can have multiple todos, every todo has 1 project and is tied to 1 user. 

These are the requirements: 

*Project*

    - A project can be created

    - A project has a name

    - A project can have multiple todos

    - A list of projects can be viewed


*ToDo*

    - A todo can be created

    - A todo has a description

    - A todo can have 2 states - "Todo" and "Done"

    - A todo is related to 1 project

    - A todo is owned by 1 user

    - A todo keeps track of how many times it's viewed (simple counter)

    - A todo can be marked as done

    - A todo can be deleted

    - A todo can be viewed

    - A list of todos can be viewed



## Considerations

    - Simple register and login were created

    - Within registering function - the "confirmed" validator could have been used to simplify the validator

    - The authorization on projects was not within scope of the asignment, yet felt important

    - Soft deletes were used, this renders the foreign key cascades useless since we are not force deleting trashed items, but for future purposes the cascade should help

    - Authorization is normally done with scopes, for sake of simplicity this is now done within the controllers

    - Sail was used to have the local environment containerized

    - Project could be migrated to Laravel 11


